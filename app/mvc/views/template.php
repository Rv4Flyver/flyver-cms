<!DOCTYPE html>
<html lang="ru">
<head>
    <base href="http://netbeans.ide/MVC/">
        <!--[if IE]><script type="text/javascript">
        // Fix for IE ignoring relative base tags.
        (function() {
            var baseTag = document.getElementsByTagName('base')[0];
            baseTag.href = baseTag.href;
        })();
    </script><![endif]-->  
    <meta charset="utf-8">
    <title>Главная</title>
    <link rel="stylesheet" href="/mvc/template/css/style.css">

</head>
<body>
    <?php include "app/mvc/views/".$content.".php"; ?>
</body>
</html>