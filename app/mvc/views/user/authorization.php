<div id="modalWrapper">
    <div id="modalDialog">
        <form name="LogInForm" id="LogInForm" action="app/core/login.php" enctype="multipart/form-data" method="post">
	    <div>
                <?php echo Dictionary('login') ?>:
                <input name="login" type="text" placeholder="<?php echo Dictionary('enter_username') ?>">
            </div>
            <div>
                <?php echo Dictionary('password') ?>:
                <input name="password" type="text" placeholder="<?php echo Dictionary('enter_password') ?>">
            </div>
            <div>
                <button type="submit"><?php echo Dictionary('log_in') ?></button> <?php echo Dictionary('or') ?> <a href="user/registration"><?php echo Dictionary('register') ?></a>
            </div>
        </form>
    </div>
</div>