<div id="modalWrapper">
    <div id="modalDialog">
        <form name="RegistrationForm" id="RegistrationForm" action="app/core/registration.php" enctype="multipart/form-data" method="post">
            <div>
                <?php echo Dictionary('login') ?>:<span id="loginAvaliability"></span>
                <input name="login" type="text" placeholder="<?php echo Dictionary('enter_username') ?>">
            </div>
            <div>
                <?php echo Dictionary('password') ?>:
                <input name="password" type="text" placeholder="<?php echo Dictionary('enter_password') ?>">
            </div>
            <div>
                <?php echo Dictionary('email') ?>:
                <input name="email" type="text" placeholder="<?php echo Dictionary('enter_email') ?>">
            </div>
            <div>
                <?php echo Dictionary('personal_info') ?>:
                <textarea name="personal_info" placeholder="<?php echo Dictionary('enter_info') ?>" rows="5"></textarea>
            </div>
            <div>
                <?php echo Dictionary('country') ?>:
                <input name="country" type="text" placeholder="<?php echo Dictionary('enter_country') ?>">
            </div>     
            <div>
                <?php echo Dictionary('city') ?>:
                <input name="city" type="text" placeholder="<?php echo Dictionary('enter_city') ?>">
            </div>    
            <div>
                <?php echo Dictionary('avatar') ?>:
                <input name="avatar" type="file" accept="image/jpeg,image/png,image/gif">
            </div> 

            <div>
                <button type="submit"><?php echo Dictionary('register') ?></button> <?php echo Dictionary('or') ?> <a href="user/authorization"><?php echo Dictionary('log_in') ?></a>
                </div>
            </form>
    </div>
</div>