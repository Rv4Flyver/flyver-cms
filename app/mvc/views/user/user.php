<div id="modalWrapper">
    <div id="modalDialog">
        <?php

            if(isset($_SESSION['id']))
            {
                ?>
        
                <div class="avatar_wrapper"><img class="avatar" src="images/avatars/<?php echo $data->avatar ?>"></div>
                <table class="userInfo">
                    <tr>
                        <td><?php echo Dictionary('login') ?>:</td><td><?php echo $data->login ?></td>
                    </tr>
                    <tr>
                        <td><?php echo Dictionary('email') ?>:</td><td><?php echo $data->email ?></td>
                    </tr>
                    <tr>
                        <td><?php echo Dictionary('country') ?>:</td><td><?php echo $data->country ?>,<?php echo $data->city ?></td>
                    </tr>
                    <tr>
                        <td><?php echo Dictionary('registration_date') ?>:</td><td><?php echo $data->registration_date ?></td>
                    </tr>
                    <tr>
                        <td><?php echo Dictionary('last_access_date') ?>:</td><td><?php echo $data->last_access_date ?></td>
                    </tr>
                </table>


                <?php //}
                ?>
                <div class="personalInfo"><?php echo $data->personal_info ?></div>

            <?php
                
            }
            else 
            {
                echo "Hello guest!";
            }
        ?>
    </div>
</div>