<?php

class User extends Controller
{
    function index()
    {
        
        include_once('app/core/userdata.php');
        
        $this->view->generate("user",NULL,$user);
        
    }
    
    function registration()
    {
        $this->view->generate("registration");
    }
    
    function authorization()
    {
        $this->view->generate("authorization");
    }
}