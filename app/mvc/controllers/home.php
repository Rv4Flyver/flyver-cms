<?php

class Home extends Controller
{
    function index()
    {
        $this->view->generate("home");
    }
    
    function registration()
    {
        $this->view->generate("registration");
    }
}