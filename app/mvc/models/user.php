<?php

class UserModel extends Model
{
    public $login;
    public $email;
    public $country;
    public $city;
    public $personal_info;
    public $registration_date;
    public $last_access_date;
    public $avatar;


    function __construct($login,$email,$country,$city,$personal_info,$registration_date,$last_access_date,$avatar) 
    {
        $this->login = $login;
        $this->email = $email;
        $this->country = $country;
        $this->city = $city;        
        $this->personal_info = $personal_info;
        $this->registration_date = $registration_date;                                
        $this->last_access_date = $last_access_date; 
        $this->avatar = $avatar;
    }
}

