<?php

class View
{
    protected $default_template;
    protected $controller;
    
    
    function __construct($controller) 
    {
        $this->controller = $controller;
        $this->default_template = "template";
    }
            
    function generate($content, $template = NULL, $data = NULL)
    {
        $content = $this->controller."/".$content;
        
        if(is_array($data))
        {
            extract($data);
        }
        
        try
        {
            if(!is_null($template))
            {
                include_once "app/mvc/views/".$template.".php";
            }
            else 
            {
                include_once "app/mvc/views/".$this->default_template.".php";            
            }
        }
        catch(Exception $ex)
        {
            echo $ex->getMessage();
        }
    }
}