<?php 
ob_start();
session_set_cookie_params(100500);
session_start();
header('Content-type: text/html; charset=utf-8');
try
{
    require_once 'mvc/model.php';
    require_once 'mvc/view.php';
    require_once 'mvc/controller.php';
    require_once 'core/router.php';
    require_once 'core/dictionary.php';
}
catch(Exception $ex)
{
    echo $ex;
}
Router::run();