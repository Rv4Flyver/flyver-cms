<?php

    $parameters = parse_ini_file("config.ini");
    $mysql_host = $parameters['db_host'];
    $mysql_user = $parameters['db_user']; 
    $mysql_password = $parameters['db_password'];
    $db_name = $parameters['db_name'];
    $table_name = "users";
    
    mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
    $db = new mysqli($mysql_host, $mysql_user, $mysql_password, $db_name);
    