<?php
    ob_start();
    session_set_cookie_params(100500);
    session_start();

    $redirectLocation = "Location: /mvc/user";

    require_once("inputdata.php");
    require_once("connection.php");
    
    $haveErrors = false;

    if(CheckLoginErrors())
    {
        $redirectLocation = "Location: ../error.php?".http_build_query(CheckLoginErrors());
        $haveErrors = true;
    }   
    if($db->connect_errno > 0)
    {
        error_log('Unable to connect to database [' . $db->connect_error . ']');
        $haveErrors = true;
    }   

    if(!$haveErrors)
    {
        $isCorrect = false;
        
        // Check if the user exists
        $qSelect = $db->prepare("SELECT id FROM  ".$table_name. " WHERE login=? AND password=? LIMIT 1");
        $password = md5($password);
        $qSelect->bind_param("ss", $login, $password);
        $qSelect->execute();
        $qSelect->bind_result($id);
        $qSelect->fetch();
        $qSelect->close();

        // updating last access date of user
        $qSelect = $db->prepare("UPDATE ".$table_name." SET last_access_date=? WHERE id=?");
        $qSelect->bind_param("si", $last_access_date, $id);
        $qSelect->execute();
        $qSelect->close();

        if($id != NULL)
        {
            $_SESSION['id'] = $id;
            $_SESSION['ip'] = $_SERVER['REMOTE_ADDR'];
        }
        else 
        {
            $redirectLocation = "Location: ../error.php?user=error_nouser";
        }
    }
    
    header($redirectLocation);