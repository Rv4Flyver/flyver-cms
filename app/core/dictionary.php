<?php
    function Dictionary($word)
    {
        $languages_dir = "lang/";
        $language_file = $languages_dir."en.json";  // default language file

        // check if there is a cookie containing language information 
        if(isset($_COOKIE["language"]))
        {
            $lang = $_COOKIE["language"];
            $language_file = $languages_dir."".$lang.".json";
        }

        try
        {
            // getting language file
            if(file_exists($language_file))
            {
                $file = file_get_contents($language_file);
            }

            // if could not read language file make a record in log and return dictionary key as a result
            if(!$file)
            {
                error_log("Cannot receive language file content.");
                return $word;
            }

            $dictionary = json_decode($file, true);

            //check if json was successfully decoded
            if($dictionary)
            {
                //if there is no corresponding value, dictionary key is used as a result
                return $dictionary[$word]?$dictionary[$word]:$word;
            }
            else
            {
                error_log("Wrong json format.");
                return $word;
            }
        }
        catch(Exception $ex)
        {
            // log exception
            return $word;
        }
    }


