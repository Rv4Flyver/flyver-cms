<?php

    $login              = isset($_POST['login']) ? $_POST['login'] : NULL;
    $password           = isset($_POST['password']) ? $_POST['password'] : NULL;
    $email              = isset($_POST['email']) ? $_POST['email'] : NULL;
    $personal_info      = isset($_POST['personal_info']) ? $_POST['personal_info'] : NULL;
    $country            = isset($_POST['country']) ? $_POST['country'] : NULL;
    $city               = isset($_POST['city']) ? $_POST['city'] : NULL;
    $last_access_date   = date("Ymd");

    mb_regex_encoding("UTF-8");
    mb_internal_encoding("UTF-8");
    function CheckLoginErrors()
    {
        global $login;
        global $password;
        $errors = array();
        
        // check login
        if($login == NULL) 
        {
            $errors["login_empty"] = "error_empty";
        }
        else {
            if(strlen($login)>22)
            {
                $errors["login_long"] = "error_login_long";
            }
            
            if(strlen($login)<4)
            {
                $errors["login_short"] = "error_login_short";
            }
            
            if(preg_match("/[^A-Za-z0-9]/",$login))
            {
                $errors["login_incorrect"] = "error_specialsymbols";                
            }
        }
       
        // check password
        if($password == NULL) 
        {
            $errors["password_empty"] = "error_empty";
        }
        else {
            if(strlen($password)>22)
            {
                $errors["password_long"] = "error_password_long";
            }
            
            if(strlen($password)<6)
            {
                $errors["password_long"] = "error_password_short";
            }
            
            if(preg_match("/[^A-Za-z0-9_]/",$password))
            {
                $errors["password_incorrect"] = "error_password";                
            }
        }
        
        return count($errors)==0 ? NULL : $errors;
    }
    
    function CheckRegistrationErrors()
    {
        global $email, $personal_info, $country, $city;
        
        // check login and password
        $errors = CheckLoginErrors();
        
        // check email
        if(strlen($email)>22)
        {
            $errors["email_long"] = "error_email_long";
        }
        
        if(!preg_match("/^[A-Za-z_]{2,10}.?[A-Za-z_0-9]{2,10}@[A-Za-z_]+?\.[A-Za-z]{2,3}$/", $email))
        {
            $errors["email_incorrect"] = "error_email";
        }
        
        // check personal_info
        if(strlen($personal_info)>1000)
        {
            $errors["personal_info_long"] = "error_maxsize";            
        }
        
        // check country
        if(strlen($country)>16)
        {
            $errors["country_long"] = "error_maxsize";            
        }     
        
        if(preg_match("/[^\w]/u", $country))
        {
            $errors["country_incorrect"] = "error_country";            
        }   
        
        if(preg_match("/\d/u", $country))
        {
            $errors["country_incorrect"] = "error_country";            
        }   
        
        // check city
        if(strlen($city)>16)
        {
            $errors["city_long"] = "error_maxsize";            
        }       
        
        if(preg_match("/[^\w]/u", $city))
        {
            $errors["city_incorrect"] = "error_city";            
        }   
        
        if(preg_match("/\d/u", $city))
        {
            $errors["city_incorrect"] = "error_city";            
        }   
        
        return count($errors)==0 ? NULL : $errors;
    }
