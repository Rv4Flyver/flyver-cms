<?php
    ob_start();
    session_start();

    $redirectLocation = "Location: ../";
    $errors = array();
    
    require_once("inputdata.php");
    require_once('upload.php');
    require_once("connection.php");
    
    $haveErrors = false;
    if(CheckRegistrationErrors())
    {
        $redirectLocation = "Location: ../error.php?".http_build_query(CheckRegistrationErrors());
        $haveErrors = true;
    }   
    if($db->connect_errno > 0)
    {
        error_log('Unable to connect to database [' . $db->connect_error . ']');
        $haveErrors = true;
    }   
    if(!$haveErrors)
    {
        // Check if the same login exists
        $isExist = false;
        $qSelect = $db->prepare("SELECT login FROM ".$table_name. " WHERE login=? LIMIT 1");
        $qSelect->bind_param("s", $login);
        $qSelect->execute();
        $qSelect->bind_result($loginCheckResult);
        $qSelect->fetch();
        $qSelect->close();
        
        var_dump($email);
        // Check if the same email exists
        $qSelect = $db->prepare("SELECT id FROM ".$table_name. " WHERE email=? LIMIT 1");
        $qSelect->bind_param("s", $email);
        $qSelect->execute();
        $qSelect->bind_result($emailCheckResult);
        $qSelect->fetch();
        $qSelect->close();
        
        if($loginCheckResult != NULL || $emailCheckResult != NULL)
        {
            $isExist = true;
        }
        
        $avatar = UploadAvatar($_FILES["avatar"], $login, $parameters['avatars_dir']);
        
        if(!$isExist)
        {
            $password = md5($password);
            $personal_info = $db->escape_string($personal_info);
            $registration_date = date("Ymd");
                    
            $stmt = $db->prepare("INSERT INTO ".$table_name. "(login, password, email, personal_info, country, city, registration_date, last_access_date, avatar) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)");
            $stmt->bind_param("sssssssss", $login, $password, $email, $personal_info, $country, $city, $registration_date, $last_access_date, $avatar);
            $stmt->execute();
            $stmt->close();
            
            
            $qSelect = $db->prepare("SELECT id FROM  ".$table_name. " WHERE login=? LIMIT 1");
            $qSelect->bind_param("s", $login);
            $qSelect->execute();
            $qSelect->bind_result($id);
            $qSelect->fetch();
            $qSelect->close();
        
            $_SESSION['id'] = $id;
            $_SESSION['ip'] = $_SERVER['REMOTE_ADDR'];
        }
        else 
        {
            $redirectLocation = "Location: ../error.php";
            if($loginCheckResult != NULL || $emailCheckResult != NULL)
            {
                $redirectLocation .= "?user_error=error_user_inuse";
            }
        }
    }
    
    header($redirectLocation);