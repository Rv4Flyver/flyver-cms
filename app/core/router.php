<?php

class Router
{
    static function run()
    {
        $controller = "home";
        $action = "index";
        $model = $controller;
        
        $config = parse_ini_file("config.ini");
        $routes_start = $config["base_folder_depth"];
        
        $routes = explode("/", $_SERVER["REQUEST_URI"]);
        
        // getting controller name or leaving it to default
        if(!empty($routes[$routes_start]))
        {
            $controller = $routes[$routes_start];
        }
        
        // getting action name or leaving it to default
        if(!empty($routes[++$routes_start]))
        {
            $action = $routes[$routes_start];
        }
        
        try
        {
            // including model, if exists
            $file = "app/mvc/models/".strtolower($controller).".php";
            if(file_exists($file))
            {
                include_once $file;
            }
            
            
            $file = "app/mvc/controllers/".strtolower($controller).".php";
            
            if(file_exists($file))
            {
                include_once $file;
            }
            else 
            {
                throw new Exception("Controller do not exists at ".$file);
            }
            
            $controller = new $controller;
            
            if(method_exists($controller, $action))
            {
                $controller->$action();
            }
            else 
            {
                 throw new Exception("Action ".$action." do not exists at ".$file);               
            }
        }
        catch(Exception $ex)
        {
            echo $ex->getMessage();
            //Router::error404();
        }
    }
    
    function error404()
    {
        $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
        header('Location:'.$host.'404');
    }
}
