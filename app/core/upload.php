<?php
    function UploadAvatar($file, $login, $upload_dir)
    {
        $default="default.png";
        
        if($file == NULL)
        {
            return $default;
        }
        // Avatar upload
        $upload_file= basename($file["name"]);
        $imageFileType = pathinfo($upload_dir.$upload_file,PATHINFO_EXTENSION);

        if ($file["size"] > 500000) {
            return $default;
        }
        
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
            return $default;
        }
        
        if (move_uploaded_file($file["tmp_name"], $upload_dir.$login.".".$imageFileType)) {
            echo "The file ". basename( $file["name"]). " has been uploaded.";
        } 
        else
        {
            return $default;
        }
        
        return $login.".".$imageFileType;
    }
    



